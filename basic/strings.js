/* Properties */

let string = "Hola mundo!";

console.log(string.length);
console.log(string.toUpperCase());
console.log(string.toLowerCase());

let stringMayus = string.toUpperCase();
console.log(stringMayus);

console.log(string.indexOf('H'));
console.log(string.replace('Hola', 'Napster'));
console.log(string.substring(4, 6));
console.log(string.slice(-5));

let string2 = "Youtuber, string working   ";

console.log(string2.trim());
console.log(string2.startsWith('Y', 5));
console.log(string2.endsWith(' '));

console.log(string2.includes('o', 2));

let string3 = "Hi!";

console.log(string3.repeat(5));

/* Template strings */

let name = "Napster";
let age = 20;

console.log('Hi ' + name + ' with ' + age + ' years');

console.log(`Hi ${name} with ${age + 4} years.`);
