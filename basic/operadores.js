/* Basic operators */
let a = 5;
let b = 4;

let user = 'Napster';

console.log(a + b);
console.log(a - b);
console.log(a * b);
console.log(a / b);
console.log(a % b);

console.log("Hi, " + user);

/* Preincrement / postincrement */

console.log(a++);
console.log(++a);
console.log(a);

console.log(b++);
console.log(++b);
console.log(b);


console.log(a--);
console.log(--a);
console.log(a);

console.log(b--);
console.log(--b);
console.log(b);

/* Assing operators */
a += 3;
a -= 3;

a *= 3;
a /= 3;

a %= 3;
console.log(a);