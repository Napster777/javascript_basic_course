/* Ordenar 3 numeros de a,b,c de mayor a menor*/

const numbers = document.getElementById('numbers');
const result = document.getElementById('result');

let a = prompt('Introduzca un numero');
let b = prompt('Introduzca un numero');
let c = prompt('Introduzca un numero');

numbers.textContent = `Los numeros son ${a}, ${b}, y ${c}`;

/* ejercicio */

if (a >= b && a >= c){
    if (b >= c){
        result.textContent = `El orden es: ${a}, ${b}, ${c}`;
    } else {
        result.textContent = `El orden es: ${a}, ${c}, ${b}`;
    }
} else if (b >= a && b >= c){
    if (a >= c){
        result.textContent = `El orden es: ${b}, ${a}, ${c}`;
    } else {
        result.textContent = `El orden es: ${b}, ${c}, ${a}`;
    }
} else if (c >= a && c >= b){
    if (a >= b){
        result.textContent = `El orden es: ${c}, ${a}, ${b}`;
    } else {
        result.textContent = `El orden es: ${c}, ${b}, ${a}`;
    }
}